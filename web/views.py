from django.shortcuts import render
from django.views.generic import TemplateView, FormView
from django import forms

# Create your views here.
class ErrandForm(forms.Form):
    name = forms.CharField(max_length=50)
    email = forms.EmailField(max_length=50)
    sender_number = forms.CharField(max_length=11, required=False)
    receiver_number = forms.CharField(max_length=11)
    sender_address = forms.CharField(widget=forms.Textarea, required=False)
    receiver_address = forms.CharField(widget=forms.Textarea)
    package_description = forms.CharField(widget=forms.Textarea)
    landmark = forms.CharField(max_length=50)
    errand_type = forms.CharField(max_length=50)

class IndexPage(FormView):
    template_name = 'web/index.html'
    form_class = ErrandForm

    def post(self, request, *kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.render_to_response(self.get_context_data(success=True))
        return self.render_to_response(self.get_context_data())