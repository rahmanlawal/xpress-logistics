
omit='*/migrations/*,*/tests/*,*/__init__.py,manage.py,*/apps.py,*/tests.py,*/asgi.py,*/wsgi.py'
cd src && ../venv/bin/coverage run --source="." --omit="$omit" manage.py test --settings=deliveryapi.testsettings \
        && ../venv/bin/coverage report -m --skip-covered \
        && ../venv/bin/coverage erase
